FROM python:3-alpine
ENTRYPOINT python /app/pihole_influx.py
WORKDIR /app
COPY requirements.txt ./
RUN pip3 install -r requirements.txt
COPY pihole_influx.py ./