#! /usr/bin/python

# Script originally created by JON HAYWARD: https://fattylewis.com/Graphing-pi-hole-stats/
# Adapted to work with InfluxDB by /u/tollsjo in December 2016
# Updated by Cludch December 2016

# To install and run the script as a service under SystemD. See: https://linuxconfig.org/how-to-automatically-execute-shell-script-at-startup-boot-on-systemd-linux

import requests
import os
from influxdb import InfluxDBClient

HOSTNAME = os.getenv("HOSTNAME",
                     "ns-01")  # Pi-hole hostname to report in InfluxDB for each measurement
PIHOLE_API = os.getenv("API_ADDRESS", "http://%s/admin/api.php" % HOSTNAME)
INFLUXDB_SERVER = os.getenv("INFLUX_HOST",
                            "127.0.0.1")  # IP or hostname to InfluxDB server
INFLUXDB_PORT = os.getenv("INFLUX_PORT", 8086)  # Port on InfluxDB server
INFLUXDB_USERNAME = os.getenv("INFLUX_USER", None)
INFLUXDB_PASSWORD = os.getenv("INFLUX_PASS", None)
INFLUXDB_DATABASE = os.getenv("INFLUX_DB", "piholestats")


def send_msg(domains_being_blocked, dns_queries_today, ads_percentage_today,
             ads_blocked_today):
    json_body = [
        {
            "measurement": "piholestats." + HOSTNAME.replace(".", "_"),
            "tags": {
                "host": HOSTNAME
            },
            "fields": {
                "domains_being_blocked": int(domains_being_blocked),
                "dns_queries_today": int(dns_queries_today),
                "ads_percentage_today": float(ads_percentage_today),
                "ads_blocked_today": int(ads_blocked_today)
            }
        }
    ]
    print(json_body)
    # InfluxDB host, InfluxDB port, Username, Password, database
    client = InfluxDBClient(host=INFLUXDB_SERVER, port=INFLUXDB_PORT,
                            database=INFLUXDB_DATABASE)
    # Since you can configure influx to not require a user/pass for writing to it
    if INFLUXDB_USERNAME is not None or INFLUXDB_PASSWORD is not None:
        client.set_user_password(INFLUXDB_USERNAME, INFLUXDB_PASSWORD)

    # Uncomment to create the database (expected to exist prior to feeding it data)
    # client.create_database(INFLUXDB_DATABASE)

    client.write_points(json_body)


if __name__ == '__main__':
    api = requests.get(PIHOLE_API)  # URI to pihole server api
    API_out = api.json()
    domains_being_blocked = (API_out['domains_being_blocked'])
    dns_queries_today = (API_out['dns_queries_today'])
    ads_percentage_today = (API_out['ads_percentage_today'])
    ads_blocked_today = (API_out['ads_blocked_today'])
    # print(domains_being_blocked, dns_queries_today, ads_percentage_today, ads_blocked_today)

    send_msg(domains_being_blocked, dns_queries_today, ads_percentage_today,
             ads_blocked_today)
