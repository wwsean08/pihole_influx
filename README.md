## Pihole_Influx

A couple of basic scripts for inserting pihole data into influxdb for graphing.

*pihole_influx.py* - A python script for inserting records into influxdb.

Environment variables (with defaults):
``` bash
HOSTNAME = "ns-01" # Pi-hole hostname to report in InfluxDB for each measurement
API_ADDRESS = "http://PI_HOLE_IP_ADDRESS_HERE/admin/api.php"
INFLUX_HOST = "127.0.0.1" # IP or hostname to InfluxDB server
INFLUX_PORT = 8086 # Port on InfluxDB server
INFLUX_USER = None
INFLUX_PASS = None"
INFLUX_DB = "piholestats"
```

To run pihole_influx.py from the command line without the startup script:
```bash
/usr/bin/python ./pihole_influx.py
```

### Troubleshooting
If you get the following error:
```
Traceback (most recent call last): File "./pihole_influx.py", line 11, in <module> from influxdb import InfluxDBClient
```
You'll need to install the python-influxdb module for python.  On a raspberry pi, you can do this with:
```
sudo apt-get install python-influxdb
```

Or on CentOS / RHEL:
```
yum install python-influxdb
```
---

If you get this error:
```
Traceback (most recent call last): File "./pihole_influx.py", line 8, in <module> import requests ImportError: No module named requests
```
You'll need to install the python-requests module.
